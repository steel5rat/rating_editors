import requests
import json
import data
from bs4 import BeautifulSoup

session = requests.Session()

base_address = "https://rating.chgk.info/"
team_tournaments_format = "api/teams.json/{}/tournaments"
tournament_format = "tournament/{}"
tournament_result_format = "api/tournaments.json/{}/list"


def get_team_tournaments(id):
    response = session.get(
        (base_address + team_tournaments_format).format(id)
    )
    if response.status_code != 200:
        return None
    parsed_response = json.loads(response.text)
    seasons = list(parsed_response.values())
    
    return list(t for season in seasons for t in season["tournaments"])


def get_tournament_editors(id):
    cache_found, cached_editors = data.get_editors(id)
    if cache_found:
        return cached_editors

    response = session.get(
        (base_address + tournament_format).format(id)
    )
    if response.status_code != 200:
        return None
    content = response.content.decode("utf-8")
    soup = BeautifulSoup(content, "html.parser")
    editors_cards = soup.find_all(has_card_class)
    if len(editors_cards) < 2:
        data.set_editors(id, None)
        return None
    editors = []
    for card in editors_cards[1].find_all(has_editor_mark):
        editors.append(card.find_all(lambda t: t.name == "a")[0].contents[0])

    data.set_editors(id, editors)

    return editors


def get_tournament_result(team_id, tournament_id):
    cached_results = data.get_tournaments_results(tournament_id)
    if cached_results is not None:
        results = cached_results
    else:
        response = session.get(
            (base_address + tournament_result_format).format(tournament_id)
        )
        if response.status_code != 200:
            return None
        results = json.loads(response.text)
        data.set_tournaments_results(tournament_id, results)
    
    return next((team for team in results if team["idteam"] == str(team_id)), None)


def has_card_class(tag):
    return tag.name == "div" and tag.has_attr("class") and "card" in tag["class"]


def has_editor_mark(tag):
    return tag.name == "span" and tag.has_attr("class") and "row" in tag["class"]
