from pymongo import MongoClient


def init_data(connection_string):
    global db
    db = MongoClient(connection_string).rating_editors


def get_editors(tournament_id):
    editors_data = db.editors.find_one({"_id": tournament_id})
    return editors_data is not None, editors_data["editors"] if editors_data is not None else None


def set_editors(tournament_id, editors):
    db.editors.insert_one({"_id": tournament_id, "editors": editors})


def get_tournaments_results(tournament_id):
    tournaments_data = db.tournaments_results.find_one({"_id": tournament_id})
    return tournaments_data["results"] if tournaments_data is not None else None


def set_tournaments_results(tournament_id, results):
    db.tournaments_results.insert_one({"_id": tournament_id, "results": results})


def get_team_raw_results(team_id):
    teams_data = db.team_results.find_one({"_id": team_id})
    return teams_data["results"] if teams_data is not None else None


def set_team_raw_results(team_id, results):
    db.team_results.insert_one({"_id": team_id, "results": results})