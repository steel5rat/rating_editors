# -*- coding: utf-8 -*-
import rating_api
import statistics
import data
import traceback

data.init_data("mongodb://192.168.1.50:27017/admin")


def get_team_analytics_data(team_id):
    cached_team = data.get_team_raw_results(team_id)
    if cached_team is not None:
        return cached_team

    tournaments = rating_api.get_team_tournaments(team_id)

    raw_results = [{"editor": "Среднее_значение", "deltas": []}]
    average_value = raw_results[0]
    total_number = len(tournaments)
    current_index = 1
    for tournament_id in tournaments:
        print("Loading tournaments: {}/{}".format(current_index, total_number))
        current_index = current_index + 1

        tournament = rating_api.get_tournament_result(team_id, tournament_id)
        if tournament is None or tournament.get("diff_bonus") is None:
            continue
        tournament_diff = float(tournament["diff_bonus"])

        editors = rating_api.get_tournament_editors(tournament_id)

        if editors is None:
            continue

        for editor in editors:
            saved_editor = next((rec for rec in raw_results if rec["editor"] == editor), None)
            if saved_editor is None:
                saved_editor = {"editor": editor, "deltas": []}
                raw_results.append(saved_editor)

            saved_editor["deltas"].append(tournament_diff)

        average_value["deltas"].append(tournament_diff)

    data.set_team_raw_results(team_id, raw_results)

    return raw_results


def remap_editor_data(editor_data):
    deltas = editor_data['deltas']
    return {
        "editor": editor_data["editor"],
        "total": len(deltas),
        "avg": sum(deltas) / len(deltas),
        "med": statistics.median(deltas)
    }


file = open("out.txt", "w", encoding="utf-8")


def write(text):
    file.write(text + "\n")


teams = {
    "Хронически разумные United": 6936,
    "Покорители Лиг": 27129,
    "Одушевлённые аэросани": 7864,
    "Зоопарк": 51739,
    "Сенат": 59010,
    "Подозрительные пассажиры": 50186,
    "Middle": 718,
    "Ять": 4252,
    "Великий Гретцки": 55674,
    "Легионеры Боливарии": 27119,
    "Батянский флекс": 66479,
    "Семь сорок": 55501,
    "Любовь Каксон": 42561,
    "Имитируем сарказм": 38011,
    "New-реанимация": 1375,
    "ТехноЛожка": 55813,
    "Клёк": 59514,
    "Енотики-7": 6193,
    "И пусть никто не уйдёт обиженный": 52675,
    "Эйфью": 5444,
    "Мамонтлошечка": 43300,
    "Чмоки": 71263,
    "Aspers": 5416,
    "Эстонский экспресс": 51722,
    "Этнические очистки": 51461,
    "Сон Кэтрин Мартелл": 58832,
    "Эталон этанола": 39997,
    "Умник": 1183,
}
try:
    for team_name, team_id in teams.items():
        print("Loading: " + team_name)
        team_data = get_team_analytics_data(team_id)
        print("Done loading")
        grouped_team_data = list(remap_editor_data(editor) for editor in team_data)
        cutted_off_low_totals = list(editor for editor in grouped_team_data if editor["total"] > 3)
        sorted_by_total = list(sorted(cutted_off_low_totals, key=lambda l: (-l["avg"], -l["total"])))

        write(team_name + ":")
        for editor in sorted_by_total:
            write("    {} ({}): avg {:2.2f}, med {:2.2f}".format(
                editor["editor"].split(" ")[0],
                editor["total"],
                editor["avg"],
                editor["med"]))
        write("---------------")
except:
    exception = traceback.format_exc()
    print(exception)

file.close()
